function [sig,mixedsig]=demosig();
%
% function [sig,mixedsig]=demosig();
% 
% Returns artificially generated test signals, sig, and mixed
% signals, mixedsig. Signals are row vectors of
% matrices. Input mixedsig to FastICA to see how it works.

% @(#)$Id: demosig.m,v 1.2 2003/04/05 14:23:57 jarmo Exp $

%create source signals (independent components)
N=500; %data size

v=[0:N-1];
sig=[];
sig(1,:)=sin(v/2); %sinusoid
sig(2,:)=((rem(v,23)-11)/9).^5; %funny curve
sig(3,:)=((rem(v,27)-13)/9); %saw-tooth
sig(4,:)=((rand(1,N)<.5)*2-1).*log(rand(1,N)); %impulsive noise

for t=1:4
sig(t,:)=sig(t,:)/std(sig(t,:));
end

%remove mean (not really necessary)

[sig mean]=remmean(sig);

%create mixtures

Aorig=rand(size(sig,1));
mixedsig=(Aorig*sig);

figure;
subplot(4,1,1);
plot(sig(1,:));
xlabel('n');
ylabel('幅度');
title('正弦曲线');
subplot(4,1,2);
plot(sig(2,:));
xlabel('n');
ylabel('幅度');
title('不规则曲线');
subplot(4,1,3);
plot(sig(3,:));
xlabel('n');
ylabel('幅度');
title('锯齿形曲线');
subplot(4,1,4);
plot(sig(4,:));
xlabel('n');
ylabel('幅度');
title('脉冲噪声');

figure;
subplot(4,1,1);
plot(mixedsig(1,:));
xlabel('n');
ylabel('幅度');
title('混合信号1');
subplot(4,1,2);
plot(mixedsig(2,:));
xlabel('n');
ylabel('幅度');
title('混合信号2');
subplot(4,1,3);
plot(mixedsig(3,:));
xlabel('n');
ylabel('幅度');
title('混合信号3');
subplot(4,1,4);
plot(mixedsig(4,:));
xlabel('n');
ylabel('幅度');
title('混合信号4');
