clear all;
close all;
clc;
x=demosig();

icasig = fastica(x);

figure;
subplot(4,1,1);
plot(icasig(1,:));
xlabel('n');
ylabel('幅度');
title('分离信号1');
subplot(4,1,2);
plot(icasig(2,:));
xlabel('n');
ylabel('幅度');
title('分离信号2');
subplot(4,1,3);
plot(icasig(3,:));
xlabel('n');
ylabel('幅度');
title('分离信号3');
subplot(4,1,4);
plot(icasig(4,:));
xlabel('n');
ylabel('幅度');
title('分离信号4');